<p align="center">
  <img src="literalJS.png" alt="LiteralJS Logo">
</p>

---
# LiteralJS Router
### A small JavaScript router for the LiteralJS framework.

### Links
- **Documentation**: https://literaljs.com

Recent 0.0.6 microbundle:
```js
$ microbundle
Build output to build:
        547 B: index.js
        566 B: index.m.js
        608 B: index.umd.js
```
