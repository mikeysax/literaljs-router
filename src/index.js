let HistoryListenerAdded = false;
let PopstateTriggered = false;
let InitialRender = true;
let LastLocation;

export function Router({ path, routes, key = 'location', set }) {
	if (!HistoryListenerAdded) {
		HistoryListenerAdded = true;
		window.onpopstate = function(e) {
			PopstateTriggered = true;
			const location = e.target.location;
			set({ [key]: location.pathname + location.search + location.hash });
		};
	}

	if (!path || path === location.pathname) {
		// Loads the path and params if typed and provided initially
		path = location.pathname + location.search + location.hash;
	}

	const hrefParser = document.createElement('a');
	hrefParser.href = location.origin + path;
	const incomingPath =
		hrefParser.pathname + hrefParser.search + hrefParser.hash;

	let notFound;
	routes = routes.find(route => {
		if (typeof route.path === 'string') {
			if (route.path.match(/^404$/)) {
				// Find the 404 component
				notFound = route;
			}

			route.path = new RegExp('^' + route.path + '$', 'i');
		}

		return hrefParser.pathname.match(route.path);
	});

	// Determines If New URL Should Be Added To History
	if (PopstateTriggered) {
		PopstateTriggered = false;
	} else {
		if (InitialRender) {
			InitialRender = false;
		} else if (LastLocation !== incomingPath) {
			history.pushState({}, null, incomingPath);
		}
	}

	LastLocation = incomingPath;

	// Render Correct Page
	if (typeof routes === 'object' && routes.render) {
		return routes.render();
	} else if (notFound) {
		return notFound.render();
	} else {
		return {
			element: 'div',
			attributes: {
				class: 'not-found'
			},
			children: ['Not Found']
		};
	}
}

export function getParams() {
	if (!location.search) {
		return {};
	}
	const finalParams = {};
	const searchSplit = location.search.substring(1).split('&');
	let currentParamSplit;
	for (let i = 0; i < searchSplit.length; i++) {
		currentParamSplit = searchSplit[i].split('=');
		finalParams[currentParamSplit[0]] = currentParamSplit[1];
	}
	return finalParams;
}

const LiteralJsRouter = { Router, getParams };
export default LiteralJsRouter;
